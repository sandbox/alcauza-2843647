<?php
/**
 * @file
 * This file provides administration form for the module.
 */

/**
 * Provides form for gsd admin.
 */
function gsd_admin_form($form_state) {

  $form['gsd'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global Google Structured Data')
    );
  $form['gsd']['gsd_enable'] = array (
    '#type' => 'checkbox',
    '#title' => t('Enable GSD'),
    '#default_value' => variable_get('gsd_enable',false),
    );


  $form['gsdwebsite'] = array(
    '#type' => 'fieldset',
    '#title' => t('Website features'),
    '#states' => array(
      'invisible' => array(
        'input[name="gsd_enable"]' => array('checked' => FALSE),
        ),
      ),
    );
  $form['gsdwebsite']['gsd_website'] = array (
    '#type' => 'checkbox',
    '#title' => t('Enable GSD website info'),
    '#description' => 'Use structured data markup on your official website to indicate the preferred name you want Google Search to display in Search results. Provided names must be reasonably similar to your domain name.
Use natural names to refer to the site, such as Google, rather than Google, Inc.
The name must be unique to your site — not used by any other site.
Not a misleading description of your site. See <a href="https://developers.google.com/search/docs/data-types/sitename">official docs</a> for more info.',
    '#default_value' => variable_get('gsd_website',false),
    );
  $form['gsdwebsite']['settings'] = array(
    '#type' => 'container',
    '#states' => array(
      'invisible' => array(
        'input[name="gsd_website"]' => array('checked' => FALSE),
        ),
      ),
    );
  $form['gsdwebsite']['settings']['gsd_website_name'] = array (
    '#type' => 'textfield',
    '#title' => t('Site name'),
    '#default_value' => variable_get('gsd_website_name', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Enter the name of this website as you want Google to show it.'),
  );
  $form['gsdwebsite']['settings']['gsd_website_url'] = array (
    '#type' => 'textfield',
    '#title' => t('Site url'),
    '#default_value' => variable_get('gsd_website_url', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Enter the url of this website.'),
  );
  $form['gsdwebsite']['settings']['gsd_website_alt'] = array (
    '#type' => 'textfield',
    '#title' => t('Alternate name'),
    '#default_value' => variable_get('gsd_website_alt', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Enter an alternate name for this website. This feature is optional'),
  );

  // Search feature form

  $form['gsdwebsite']['gsdsearch'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sitelinks Searchbox features'),
    '#states' => array(
      'invisible' => array(
        'input[name="gsd_website"]' => array('checked' => FALSE),
        ),
      ),
    );
  $form['gsdwebsite']['gsdsearch']['gsd_search'] = array (
    '#type' => 'checkbox',
    '#title' => t('Enable GSD Sitelinks Searchbox'),
    '#description' => 'Search users often perform navigational searches using product or brand names. Then, on the product or company website, they perform a second search for specific details. Using sitelinks searchbox markup in your content lets the user perform the second search without leaving the original, navigational search results. See <a href="https://developers.google.com/search/docs/data-types/sitelinks-searchbox">official docs</a> for more info.',
    '#default_value' => variable_get('gsd_search',false),
    );
  $form['gsdwebsite']['gsdsearch']['settings'] = array(
    '#type' => 'container',
      '#states' => array(
        'invisible' => array(
          'input[name="gsd_search"]' => array('checked' => FALSE),
        ),
      ),
  );

  $form['gsdwebsite']['gsdsearch']['settings']['gsd_search_action_url'] = array (
    '#type' => 'textfield',
    '#title' => t('Search query url'),
    '#default_value' => variable_get('gsd_search_action_url', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Enter the url of your search query. E.g. "https://query.example.com/search?q='),
  );

  // Social Profiles form



  $form['gsdwebsite']['gsdsocial'] = array(
    '#type' => 'fieldset',
    '#title' => t('Social profiles feature'),
    '#states' => array(
      'invisible' => array(
        'input[name="gsd_website"]' => array('checked' => FALSE),
        ),
      ),
    );
  $form['gsdwebsite']['gsdsocial']['gsd_social'] = array (
    '#type' => 'checkbox',
    '#title' => t('Enable social profiles'),
    '#description' => 'Use markup on your official website to add your social profile information to a Google Knowledge panel. Knowledge panels prominently display your social profile information in some Google Search results.<br> Supported Social Profiles

Use structured data markup embedded in your public website to specify your preferred social profiles. You can specify these types of social profiles:

<strong>Facebook
Twitter
Google+
Instagram
YouTube
LinkedIn
Myspace
Pinterest
SoundCloud
Tumblr.</strong> <br> More info: <a href="https://developers.google.com/search/docs/data-types/social-profile-links" target="_blank">https://developers.google.com/search/docs/data-types/social-profile-links</a>',
    '#default_value' => variable_get('gsd_social',false),
    );
  $form['gsdwebsite']['gsdsocial']['settings'] = array(
    '#type' => 'container',
      '#states' => array(
        'invisible' => array(
          'input[name="gsd_social"]' => array('checked' => FALSE),
        ),
      ),
  );
  $form['gsdwebsite']['gsdsocial']['settings']['gsd_social_type'] = array (
    '#type' => 'select',
    '#options' => drupal_map_assoc(array('Person', 'Organization')),
    '#title' => t('Type'),
    '#default_value' => variable_get('gsd_social_type', ''),
    '#description' => t('Choose if your website is for a Person or Organization'),
  );
  $form['gsdwebsite']['gsdsocial']['settings']['gsd_social_fb_url'] = array (
    '#type' => 'textfield',
    '#title' => t('Facebook profile url'),
    '#default_value' => variable_get('gsd_social_fb_url', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Enter the url for this social profile'),
  );
  $form['gsdwebsite']['gsdsocial']['settings']['gsd_social_tw_url'] = array (
    '#type' => 'textfield',
    '#title' => t('Twitter profile url'),
    '#default_value' => variable_get('gsd_social_tw_url', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Enter the url for this social profile'),
  );
  $form['gsdwebsite']['gsdsocial']['settings']['gsd_social_gplus_url'] = array (
    '#type' => 'textfield',
    '#title' => t('Google+ profile url'),
    '#default_value' => variable_get('gsd_social_gplus_url', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Enter the url for this social profile'),
  );
  $form['gsdwebsite']['gsdsocial']['settings']['gsd_social_ig_url'] = array (
    '#type' => 'textfield',
    '#title' => t('Instagram profile url'),
    '#default_value' => variable_get('gsd_social_ig_url', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Enter the url for this social profile'),
  );
  $form['gsdwebsite']['gsdsocial']['settings']['gsd_social_yt_url'] = array (
    '#type' => 'textfield',
    '#title' => t('Youtube profile url'),
    '#default_value' => variable_get('gsd_social_yt_url', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Enter the url for this social profile'),
  );
  $form['gsdwebsite']['gsdsocial']['settings']['gsd_social_li_url'] = array (
    '#type' => 'textfield',
    '#title' => t('LinkedIn profile url'),
    '#default_value' => variable_get('gsd_social_li_url', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Enter the url for this social profile'),
  );
  $form['gsdwebsite']['gsdsocial']['settings']['gsd_social_ms_url'] = array (
    '#type' => 'textfield',
    '#title' => t('Myspace profile url'),
    '#default_value' => variable_get('gsd_social_ms_url', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Enter the url for this social profile'),
  );
  $form['gsdwebsite']['gsdsocial']['settings']['gsd_social_pi_url'] = array (
    '#type' => 'textfield',
    '#title' => t('Pinterest profile url'),
    '#default_value' => variable_get('gsd_social_pi_url', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Enter the url for this social profile'),
  );
  $form['gsdwebsite']['gsdsocial']['settings']['gsd_social_sc_url'] = array (
    '#type' => 'textfield',
    '#title' => t('Soundcloud profile url'),
    '#default_value' => variable_get('gsd_social_sc_url', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Enter the url for this social profile'),
  );
  $form['gsdwebsite']['gsdsocial']['settings']['gsd_social_tb_url'] = array (
    '#type' => 'textfield',
    '#title' => t('Tumblr profile url'),
    '#default_value' => variable_get('gsd_social_tb_url', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Enter the url for this social profile'),
  );

  return system_settings_form($form);
}


/**
 * Provides validation for gsd admin form
 * @see [URI | FQSEN] [<description>]
 */
function gsd_admin_form_validate($form, &$form_state) {
// dpm($form_state);

  if ($form_state['values']['gsd_website'] != false) {
    if (empty($form_state['values']['gsd_website_name'] )) {
      form_set_error('gsd_website', t('You must specify a name.'));
    }
    if (empty($form_state['values']['gsd_website_url'] )) {
      form_set_error('gsd_website_url', t('You must specify a url.'));
    }
  }

  if ($form_state['values']['gsd_search'] != false) {

    if (empty($form_state['values']['gsd_search_action_url'] )) {
      form_set_error('gsd_search_action_url', t('You must specify a url.'));
    }
  }

}